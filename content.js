const script = { 
	id: 'stadia_racing',
	name: 'Stadia Racing Extension'
};

chrome.storage.sync.get(['id','buttons','axes','buttonData','axisData','disabled'], (stored_data) => {
	if (stored_data.disabled) return;

	const extension = document.createElement("script");
	extension.id = `${script.id}_script`;
	extension.dataset.id = script.id;
	extension.dataset.name = script.name;
	extension.setAttribute('type', 'module');
	extension.setAttribute("src", chrome.extension.getURL(`src/${script.id}.js`));	

	// wait till the dom has loaded.
	window.addEventListener('DOMContentLoaded', () => {
		const body = document.body || document.head || document.documentElement;
		body.appendChild(extension);
	}, true);
});

window.addEventListener(`${script.id}_load`, (event) => {
	chrome.runtime.sendMessage({
		from: 'content',
		subject: 'load',
		data: event.detail
	});
});

// We have to pass a message from the background via the content js 
// so that it understands the page context.
chrome.runtime.onMessage.addListener((message, sender, callback) => {    
    if (message.from == 'background') {
		let evt = null;
		if (message.subject == 'loaded') { evt = new CustomEvent(`${script.id}_setup`, {detail: message.data }); }
		if (message.subject == 'update') { evt = new CustomEvent(`${script.id}_update`, {detail: message.data }); }
		
		if (evt) {
			window.dispatchEvent(evt);
			return callback({success: true, data: message.data});
		}
		callback({success: false, data: chrome.runtime.lastError});
    }
    callback({success: false, data: chrome.runtime.lastError});
});
