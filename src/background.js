'use strict';

import Debug from './core/debug.js';
import Config from './core/config.js';

Debug.enabled = true;
Debug.level = 0;
Debug.prefix = "SR";

const default_id = "Emulated Gamepad rev.A (STANDARD GAMEPAD Vendor: 1fba Product: 3a97)";

// Default Options
const init_data = new Config();


// set defaults when first installed.
chrome.runtime.onInstalled.addListener(function () {
  // Clear storage on install
  chrome.storage.sync.clear();
  // Setup storage
  chrome.storage.sync.get(init_data, (stored_data) => chrome.storage.sync.set(stored_data));

  chrome.declarativeContent.onPageChanged.removeRules(undefined, function () {
   chrome.declarativeContent.onPageChanged.addRules([{
      conditions: [
        new chrome.declarativeContent.PageStateMatcher({
          pageUrl: { hostEquals: 'stadia.google.com', schemes: ['https'] }
        })
      ],
      actions: [new chrome.declarativeContent.ShowPageAction()]
    }]);
  });
});

chrome.runtime.onStartup.addListener(function() {
  chrome.storage.sync.get(init_data, (stored_data) => chrome.storage.sync.set(stored_data));
});

// if any settings change, fire an event to update the content pages.
chrome.storage.onChanged.addListener((changes) => {
  chrome.tabs.query({ active: true, currentWindow: true }, (tabs) => {
    chrome.tabs.sendMessage(tabs[0].id, {
      from: 'background',
      subject: 'update',
      updates: changes
    });
  });
});

// Messages received from the page
chrome.runtime.onMessage.addListener((message, sender, response) => {
  Debug.log(message, 'received')

  if (message.from == 'content') {
    if (message.subject == 'load') {

      chrome.storage.sync.get(init_data, (stored_data) => {
        chrome.tabs.query({ active: true, currentWindow: true }, (tabs) => {
          if (tabs.length > 0) {
            chrome.tabs.sendMessage(tabs[0].id, {
              from: 'background',
              subject: 'loaded',
              data: stored_data
            });
          }
        });
      });
    }      
  }  
});
