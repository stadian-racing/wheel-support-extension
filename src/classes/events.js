export default class SREvent {
    constructor(name) {
        this.listener = `stadia_racing_${name}`;
    }

    event(data) {
        if (data == null) data = ['button','axes','disabled'];
        return new CustomEvent(`${this.listener}`, { detail:  data });
    }
}