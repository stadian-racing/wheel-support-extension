import Debug from '../core/debug.js';

export default class Gamepad {
    constructor() {      
        this.id =  "[Emulated] Gamepad rev.A (STANDARD GAMEPAD Vendor: 1fba Product: 3a97)";
        this.index = 0;
        this.connected = false;
        this.timestamp = 0;
        this.mapping = "standard";
        this.axes = [0, 0, 0, 0];
        this.buttons = new Array(19).fill().map(m => ({pressed: false, touched: false, value: 0}));
        Debug.log(this, 'init');
    }

    connect(e) {
        this.id = `[Emulated] ${e.gamepad.id}`;
        this.index = e.gamepad.index;
        this.connected = e.gamepad.connected;
        this.timestamp = e.gamepad.timestamp;  
        Debug.log(this, 'connected');
    }
    disconnect() {
        this.connected = false;
        Debug.log(this, 'disconnected');
    }
    
    get buttonCount() {
        this.buttons.length;
    }
    get axisCount() {
        this.axes.length;
    }
}