export default class GamepadInput {
	constructor(label, type, id, map) {
		this.value = (type == 'axis') ? this.#axis() : this.#button();
		this.label = label;
		this.type = type;
		this.index = 0;
		this.to = {
			id: id,
			label: label,
			index: map
		};
		this.id = id;
	}
	
	get isAxis() { return this.type == 'axis'; }
	get isButton() { return this.type == 'button'; }
	get isTrigger() { return this.type == 'trigger'; }

	#button() {
		return {pressed: false, touched: false, value: 0};
	}

	#axis() {
		return 0;
	}
}