import UIComponent from '../core/ui_component.js';

export default class Button extends UIComponent {
    constructor(parent, configButton) {
        super(parent);
        this.index = configButton.index || 0;
        this.labelText = configButton.label || "";
        this.id = `btn_${this.index}`;
        this.classes.push('button');
        this.dataset = {
            index: configButton.index, 
            label: configButton.label,
            mappedIndex: configButton.to.index,
            mappedLabel: configButton.to.label
        };
        this.label = new UIComponent(this.element);
        this.setButton = new UIComponent(this.element, 'button');
        this.resetButton = new UIComponent(this.element, 'button');
    }

    buildLabel(){
        this.label.id = `btn_${this.index}_label`;
        this.label.classes = ['label'];
        this.label.element.innerText = `ID: ${this.index} ( ${this.labelText} )`;
        return this.label.build();
    }

    buildSetButton() {
        this.setButton.id = `btn_${this.index}_set`;
        this.setButton.element.title = "Remap";
        this.setButton.classes = ['mapping','set'];
        this.setButton.element.innerHTML = '<svg xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24"><path d="M0 0h24v24H0z" fill="none"/><path d="M3 17.25V21h3.75L17.81 9.94l-3.75-3.75L3 17.25zM20.71 7.04c.39-.39.39-1.02 0-1.41l-2.34-2.34c-.39-.39-1.02-.39-1.41 0l-1.83 1.83 3.75 3.75 1.83-1.83z"/></svg>';
        return this.setButton.build();
    }

    buildResetButton(){
        this.resetButton.id = `btn_${this.index}_reset`;
        this.resetButton.element.title = "Reset Map";
        this.resetButton.classes = ['mapping','reset'];
        this.resetButton.element.innerHTML = '<svg xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24"><path d="M0 0h24v24H0z" fill="none"/><path d="M12 6v3l4-4-4-4v3c-4.42 0-8 3.58-8 8 0 1.57.46 3.03 1.24 4.26L6.7 14.8c-.45-.83-.7-1.79-.7-2.8 0-3.31 2.69-6 6-6zm6.76 1.74L17.3 9.2c.44.84.7 1.79.7 2.8 0 3.31-2.69 6-6 6v-3l-4 4 4 4v-3c4.42 0 8-3.58 8-8 0-1.57-.46-3.03-1.24-4.26z"/></svg>';
        return this.resetButton.build();
    }

    build(){
        this.buildLabel();
        this.buildSetButton();
        this.buildResetButton();
        super.build();
    }
}