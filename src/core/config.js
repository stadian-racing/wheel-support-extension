import GamepadInput from '../classes/gamepad_input.js';

export default class Config {
    constructor() {
        this.id =  "Emulated Gamepad rev.A (STANDARD GAMEPAD Vendor: 1fba Product: 3a97)";
        this.disabled = false;
        this.buttons = [
            new GamepadInput('A', 'button', this.id, 0),
            new GamepadInput('B', 'button', this.id, 1),
            new GamepadInput('X', 'button', this.id, 2),
            new GamepadInput('Y', 'button', this.id, 3),
            new GamepadInput('L1', 'button', this.id, 4),
            new GamepadInput('R1', 'button', this.id, 5),
            new GamepadInput('L2', 'trigger', this.id, 6),
            new GamepadInput('R3', 'trigger', this.id, 7),
            new GamepadInput('Menu', 'button', this.id, 8),
            new GamepadInput('Options', 'button', this.id, 9),
            new GamepadInput('L3', 'button', this.id, 10),
            new GamepadInput('R3', 'button', this.id, 11),
            new GamepadInput('DPad Up', 'button', this.id, 12),
            new GamepadInput('DPad Down', 'button', this.id, 13),
            new GamepadInput('DPad Left', 'button', this.id, 14),
            new GamepadInput('DPad Right', 'button', this.id, 15),
            new GamepadInput('Home', 'button', this.id, 16),
            new GamepadInput('Assistant', 'button', this.id, 17),
            new GamepadInput('Capture', 'button', this.id, 18)
        ];
        this.axes = [
            new GamepadInput('Left X', 'axis', this.id, 0),
            new GamepadInput('Left Y', 'axis', this.id, 1),
            new GamepadInput('Right X', 'axis', this.id, 2),
            new GamepadInput('Right Y', 'axis', this.id, 3)
        ];
    }
}