// Custom debug module for ease of development.
class Debug {
    // global debug flag
    constructor(){
        this.enabled = false;
        this.level = 2;
        this.prefix = 'stadia_racing';
    }

    get levels() {
        console.log({
            0: "info",
            1: "warning",
            2: "error"
        });
    }

    get timestamp() {
        let d = new Date();
        let hrs = (d.getHours() < 10) ? `0${d.getHours()}` : d.getHours();
        let mins = (d.getMinutes() < 10) ? `0${d.getMinutes()}` : d.getMinutes();
        let secs = (d.getSeconds() < 10) ? `0${d.getSeconds()}` : d.getSeconds();
        return `${hrs}:${mins}:${secs}`;
    }

    log(message, name, log_level = "info") {

        // don't output logs if debugging is disabled
        if (!this.enabled) { return; }
        
        let file = '';
    
        if (name != null) { 
            file = `[${name}]`; 
            if (Array.isArray(name)){
                file = '';
                name.forEach((n) => file += `[${n}]`);
            }
        }    
    
        switch(log_level) {
            case "info":
                if (this.level < 1) { console.info(`[${this.timestamp}][${this.prefix}]${file}`, message); }
                break; 
            case "warn":
                if (this.level < 2) { console.info(`%c[${this.timestamp}][${this.prefix}]${file}`, 'color: #FFB86A;', message); }
                break;
            case "error":
                if (this.level < 3) { console.info(`%c[${this.timestamp}][${this.prefix}]${file}`, 'color: #EF83A9;', message); }
                break;  
    
            default:
                console.log(`[${this.timestamp}][${this.prefix}]${file}`, message);
        }
    }

}

export default (new Debug);
