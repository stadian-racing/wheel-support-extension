export default class UIComponent {
    constructor(parent, type) {
        this.parent = parent;
        this.element = document.createElement(type ? type : 'div');
        this.id = "";
        this.dataset = {};
        this.classes = [];
    }

    addDataAttributes(attributes) {
        for (const [key, value] of Object.entries(attributes)) {
            this.element.dataset[key] = value;
        }
    }

    addClasses(classes) {
        for (const c of classes) { this.element.classList.add(c) }
    }

    build() {
        this.element.id = this.id;
        this.addClasses(this.classes);
        this.addDataAttributes(this.dataset);
        this.parent.appendChild(this.element);
    }
}