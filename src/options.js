import Button from './components/button.js';

let options = {
    config: {},
    controllers: {},
    current: {}
};

window.Options = options;

chrome.storage.sync.get(['id','buttons','axes','buttonData','axisData','disabled'], (stored_data) => {
    options.config = stored_data;
    options.controllers = {}
});

let haveEvents = 'GamepadEvent'in window;
let haveWebkitEvents = 'WebKitGamepadEvent'in window;

function connecthandler(e) {
    addgamepad(e.gamepad);
}
function addgamepad(gamepad) {
    options.controllers[gamepad.index] = gamepad;
    options.current = gamepad;

    var t = document.createElement("h3");
    t.dataset.gamepadIndex = gamepad.index;
    t.style.textAlign = 'center';
    t.style.paddingTop = '20px';
    t.appendChild(document.createTextNode("Gamepad: " + gamepad.id));
    document.body.appendChild(t);

    var d = document.createElement("div");
    d.setAttribute("id", "controller_" + gamepad.index);


    var b = document.createElement("div");
    b.className = "buttons";

    gamepad.buttons.forEach((_button, index) => {         
        options.config.buttons[index].index = index;
        options.config.buttons[index].id = gamepad.id;
        addButton(b, index) 
    });

    d.appendChild(b);

    var a = document.createElement("div");
    a.className = "axes";

    let joysticks = 0;

    for (let i = 0; i < gamepad.axes.length; i++) {
        options.config.axes[i].index = i;
        options.config.axes[i].id = gamepad.id;

        if (i % 2 == 0 && i != gamepad.axes.length) {
            joysticks += 1
            a.appendChild(buildJoystickElement(joysticks, ['x', 'y']));
        }
    }
    d.appendChild(a);
    document.getElementById("start").style.display = "none";
    document.body.appendChild(d);
    chrome.storage.sync.set(options.config, () => console.log('mappings updated'))
    window.requestAnimationFrame(updateStatus);
}

function disconnecthandler(e) {
    removegamepad(e.gamepad);
}

function removegamepad(gamepad) {
    var d = document.getElementById("controller_" + gamepad.index);
    document.body.removeChild(d);
    delete options.controllers[gamepad.index];
}

function updateStatus() {
    scangamepads();
    for (const j in options.controllers) {
        let controller = options.controllers[j];
        let d = document.getElementById("controller" + j);
        for (let i = 0; i < controller.buttons.length; i++) {
            let remap = options.config.buttons[i].to.index;
            var b = document.getElementById(`btn_${i}`);
            var val = controller.buttons[i];

            if (remap !== i) { val = controller.buttons[remap]; }

            var pressed = val == 1.0;
            var touched = false;
            if (typeof (val) == "object") {
                pressed = val.pressed;
                if ('touched'in val) {
                    touched = val.touched;
                }
                val = val.value;
            }
            if (pressed) {
                b.classList.add("pressed");
            } else {
                b.classList.remove('pressed');
            }

            if (touched) {
                b.classList.add("touched");
            } else {
                b.classList.remove('touched');
            }
        }

        let joysticks = 0;

        for (let i = 0; i < controller.axes.length; i++) {
            if (i % 2 == 0 && i != controller.axes.length) {
                joysticks += 1
                let js = document.getElementById(`joystick_${joysticks}`);
                let jsp = js.getElementsByClassName('point')[0];
                // Move the pointer
                jsp.style.left = `${(controller.axes[i + 0].toFixed(4) * 50) + 50}%`;
                jsp.style.top = `${(controller.axes[i + 1].toFixed(4) * 50) + 50}%`;
                // Fill in the values
                document.getElementById(`js_${joysticks}_axis_x`).value = controller.axes[i + 0].toFixed(4);
                document.getElementById(`js_${joysticks}_axis_y`).value = controller.axes[i + 1].toFixed(4);
            }
        }
    }
    window.requestAnimationFrame(updateStatus);
}

function scangamepads() {
    var gamepads = navigator.getGamepads ? navigator.getGamepads() : (navigator.webkitGetGamepads ? navigator.webkitGetGamepads() : []);
    for (var i = 0; i < gamepads.length; i++) {
        if (gamepads[i] && (gamepads[i].index in options.controllers)) {
            options.controllers[gamepads[i].index] = gamepads[i];
        }
    }
}

if (haveEvents) {
    window.addEventListener("gamepadconnected", connecthandler);
    window.addEventListener("gamepaddisconnected", disconnecthandler);
} else if (haveWebkitEvents) {
    window.addEventListener("webkitgamepadconnected", connecthandler);
    window.addEventListener("webkitgamepaddisconnected", disconnecthandler);
} else {
    setInterval(scangamepads, 500);
}

function remap(index, evt) {
    const padIndex = options.current.index;
    const optButton = options.config.buttons[index];
    const button = document.getElementById(`btn_${index}`);
    const from = button.dataset.index;

    const awaitInput = setInterval(() => {
        const pad = navigator.getGamepads()[padIndex];
        addMapping(button, {index: '...', label: '...'});
      
        for (const btn of pad.buttons) {
            if (btn.touched || btn.pressed) { 
                const to = pad.buttons.indexOf(btn);

                if (from != to) {    
                    set(button, from, to);
                } else {                    
                    removeMapping(document.getElementById(button.id + '_mapped')); 
                }
                
                clearInterval(awaitInput);
                break;
            }            
        }
    }, 200);
}

function set(parent, from, to){
    const button = options.config.buttons[from];
    options.config.buttons[from].to.index = to; 
    options.config.buttons[from].to.label = options.config.buttons[to].label; 

    parent.dataset.mappedIndex = to;
    parent.dataset.mappedLabel = options.config.buttons[to].label;
    addMapping(document.getElementById(parent.id), {index: to, label: options.config.buttons[to].label});
    chrome.storage.sync.set(options.config, () => console.log(`${button.label} (id: ${button.index}) set to ${button.to.label} (id: ${button.to.index})`));
}

function reset(index, evt) {
    const button = options.config.buttons[index];
    options.config.buttons[button.index].to.index = button.index;
    options.config.buttons[button.index].to.label = button.label;
    console.log({button: button, index: index});

    removeMapping(document.getElementById(`btn_${index}_mapped`));
    chrome.storage.sync.set(options.config, () => console.log(`${button.label} (id: ${button.index}) reset`));
}

function addButton(parent, index) {
    const btnClass = new Button(parent, options.config.buttons[index]);
    btnClass.setButton.element.addEventListener('click', remap.bind(null, index), false);
    btnClass.resetButton.element.addEventListener('click', reset.bind(null, index), false);
    btnClass.build();

    if (options.config.buttons[index].to.index != options.config.buttons[index].index) {
        addMapping(btnClass.element, {index: options.config.buttons[index].to.index, label: options.config.buttons[index].to.label})
    }

    return parent;
}

function addMapping(parent, mapping) {
    let mapped = document.getElementById(parent.id + '_mapped');
    if (!mapped) {
        mapped = document.createElement('div');
        mapped.id = parent.id + '_mapped';
        mapped.className = 'mapped';
    }

    mapped.innerText = `set to ID: ${mapping.index} ( ${mapping.label} )`;

    parent.style.backgroundColor = "#ff9f1a";
    parent.firstChild.appendChild(mapped);
}

function removeMapping(elem) {
    if (elem) { 
        elem.parentNode.parentNode.style.backgroundColor = "#c2c2c2";
        elem.parentNode.removeChild(elem); 
    }    
}

function buildJoystickElement(side, axes) {
    let js = document.createElement('div');
    js.id = `joystick_${side}`;
    js.classList.add('joystick', `${side}`);

    let point = document.createElement('div');
    point.classList.add('point');
    point.style.top = "50%";
    point.style.left = "50%";

    for (let ax in axes) {
        let axis_label = document.createElement('label');
        axis_label.for = `js_${side}_axis_${axes[ax]}`;
        axis_label.innerText = `Axis ${axes[ax].toUpperCase()}`;
        let axis_input = document.createElement('input');
        axis_input.id = `js_${side}_axis_${axes[ax]}`;
        axis_input.value = 0;
        js.appendChild(axis_label);
        js.appendChild(axis_input);
    }

    js.appendChild(buildAxisElement('x'));
    js.appendChild(buildAxisElement('y'));
    js.appendChild(point);

    return js;
}

function buildAxisElement(axis) {
    let axis_guide = document.createElement('div');

    axis_guide.classList.add(`${axis}`, 'guide');

    return axis_guide;
}
