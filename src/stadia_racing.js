import EmulatedGamepad from './classes/gamepad.js';
import SREvent from './classes/events.js';
import Debug from './core/debug.js';

Debug.enabled = true;
Debug.level = 0;

Debug.log("Stadia Racing: loading...");

const emulatedGamepad = new EmulatedGamepad();
let config = {};

window.addEventListener('gamepadconnected', (event) => { emulatedGamepad.connect(event) });
window.addEventListener('gamepaddisconnected', emulatedGamepad.disconnect);

const events = {
    load: new SREvent('load'),
    setup: new SREvent('setup'),
    update: new SREvent('update')
}

window.dispatchEvent(events.load.event());
window.addEventListener(events.setup.listener, (event)=>{
    config = event.detail;
    Debug.log("Stadia Racing: running setup...");

    const gg = navigator.getGamepads;
    navigator.getGamepads = () => {
        let pads = gg.apply(navigator);
        let gamepads = Array.from(pads).filter((gp) => { return gp !== null});

        if (gamepads.length > 0) { 
            updateGamepad(gamepads[0]);
            return [emulatedGamepad, null, null, null];
        }
        return [null, null, null, null];
    }    

    window.StadiaRacing = {
        gamepad: emulatedGamepad,
        config: config,
        debug: Debug
    };

    Debug.log(window.StadiaRacing, "loaded");
    console.info(`%c[stadia_racing]`, 'color: #FFB86A;', "Extension config accessible via window.StadiaRacing");
});

function updateGamepad(gamepad){ 
    // Mapping and config changes need to be applied here
    for (const [index, button] of config.buttons.entries()) {         
        emulatedGamepad.buttons[button.to.index] = gamepad.buttons[index];
    }
    for (const [index, axis] of config.axes.entries()) {          
        emulatedGamepad.axes[index] = gamepad.axes[index];
    }
    emulatedGamepad.timestamp = gamepad.timestamp; 
}
